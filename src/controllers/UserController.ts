import { Controller } from "./Controller";
import { HttpServer } from "../server/HttpServer";
import { Request, Response } from "restify";
import { userService } from "../services/UserService";

export class UserController implements Controller {
  public initialize(httpServer: HttpServer): void {
    httpServer.get('users', this.list.bind(this));
    httpServer.get('users/:id', this.getById.bind(this));
    httpServer.post('users', this.create.bind(this));
    httpServer.put('users/:id', this.update.bind(this));
    httpServer.del('users/:id', this.remove.bind(this));
  }

  private async list(req: Request, res: Response): Promise<void> {
    res.send(await userService.list());
  }

  private async getById(req: Request, res: Response): Promise<void> {
    const user = await userService.getById(req.params.id);
    res.send(user ? 200 : 404, user);
  }

  private async create(req: Request, res: Response): Promise<void> {
    res.send(await userService.create(req.body));
  }

  private async update(req: Request, res: Response): Promise<void> {
    res.send(await userService.update({ ...req.body, id: req.params.id }));
  }

  private async remove(req: Request, res: Response): Promise<void> {
    res.send(await userService.delete(req.params.id));
  }
}