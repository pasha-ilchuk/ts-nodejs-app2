import { User } from '../models/User';
import { DatabaseProvider } from '../database/config';

export class UserService {
  public async getById(id: number): Promise<User> {
    const connection = await DatabaseProvider.getConnection();
    return await connection.getRepository(User).findOne(id);
  }

  public async list(): Promise<User[]> {
    const connection = await DatabaseProvider.getConnection();
    return await connection.getRepository(User).find();
  }

  public async create(user: User): Promise<User> {
    const connection = await DatabaseProvider.getConnection();
    return await connection.getRepository(User).save(user);
  }

  public async update(user: User): Promise<User> {
    const connection = await DatabaseProvider.getConnection();
    const repo = connection.getRepository(User);
    const entity = await repo.findOne(user.id);
    entity.firstName = user.firstName;
    entity.lastName = user.lastName;
    return await repo.save(entity);
  }

  public async delete(id: number): Promise<number> {
    const connection = await DatabaseProvider.getConnection();
    return await connection.getRepository(User).delete(id);
  }
}

export const userService = new UserService();