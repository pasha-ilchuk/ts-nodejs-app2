import { ApiServer } from "./server";
import { DatabaseProvider } from "./database/config";

DatabaseProvider.configure({
  type: process.env.DATABASE_TYPY as any || 'postgres',
  database: process.env.DATABASE_NAME || 'db_name',
  username: process.env.DATABASE_USERNAME || 'username',
  password: process.env.DATABASE_PASSWORD || 'password',
  host: process.env.DATABASE_HOST || 'localhost',
  port: +process.env.DATABASE_PORT || 5432,
  ssl: !!process.env.USE_SSL
});

const server = new ApiServer();
server.start(+process.env.PORT || 3000);